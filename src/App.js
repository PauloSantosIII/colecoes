import React, {useState} from 'react'
import {Switch, Route} from 'react-router-dom'
import {Home, Characters, Pokemons, Statistics} from './pages'

function App() {
  const [characters, setCharacters] = useState([])
  const [pokemons, setPokemons] = useState([])

  return (
    <Switch>
      <Route path='/rick-and-morty/:page'>
        <Characters setCharacters={setCharacters} />
      </Route>
      <Route path='/pokemon/:offset'>
        <Pokemons setPokemons={setPokemons} />
      </Route>
      <Route path='/statistics'>
        <Statistics characters={characters} pokemons={pokemons} />
      </Route>
      <Route path='/'>
        <Home characters={characters} setCharacters={setCharacters} pokemons={pokemons} setPokemons={setPokemons} />
      </Route>
    </Switch>
  )
}

export default App

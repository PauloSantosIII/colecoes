import PageDefault from './pageDefault'
import Header from './header'
import CharacterList from './rickandmorty-list'
import Main from './main'
import Footer from './footer'
import PokemonList from './pokemon-list'

export {PageDefault, Header, CharacterList, Main, Footer, PokemonList}
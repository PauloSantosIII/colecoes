import React from 'react'
import {Link} from 'react-router-dom'
import {StyledHeader} from '../styles'
import {FcStackOfPhotos, FcCollaboration, FcPieChart} from 'react-icons/fc'

const Header = () => {
    return (
        <StyledHeader>
            <Link to='/'>
                <FcStackOfPhotos />
            </Link>
            <Link to='/rick-and-morty/1'>
                <FcCollaboration />
            </Link>
            <Link to='/statistics'>
                <FcPieChart />
            </Link>
        </StyledHeader>
    )
}

export default Header
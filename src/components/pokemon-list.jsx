import React from 'react'
import {Card} from 'antd'
import {StyledCard} from '../styles'
import {Main} from '.'
import {motion} from 'framer-motion'

const PokemonList = ({pokemons, onSelect = () => {} }) => {
    return (
        <Main>
            {pokemons &&
                pokemons.map(({name, image, species}, index) => (
                    <motion.div key={index} whileTap={{scale: 0.9, rotate: 15}} whileHover={{scale: 1.1}} transition={{duration: 2}}>
                        <StyledCard
                            hoverable
                            onClick={() => {onSelect({name, image, species})
                            }}
                            style={{width: 240}}
                            cover={<img alt='example' src={image} />}
                        >
                            <Card.Meta title={name} description={species} />
                        </StyledCard>
                    </motion.div>
            ))}
        </Main>
    )
}

export default PokemonList
import React from 'react'
import {Header, Main, Footer} from '../components'
import {Container} from '../styles'

const PageDefault = ({children}) => {
    return (
        <Container>
            <Header />
            <Main >
                {children}
            </Main>
            <Footer />
        </Container>

    )
}

export default PageDefault
import React from 'react'
import {StyledFooter} from '../styles'

const Main = ({children}) => {
    return (
        <StyledFooter>
                {children}            
        </StyledFooter>
    )
}

export default Main
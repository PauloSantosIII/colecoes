import React from 'react'
import {StyledMain} from '../styles'

const Main = ({children}) => {
    return (
        <StyledMain initial={{scale: 1.5}} animate={{scale: 1.0}}>
            {children}
        </StyledMain>
    )
}

export default Main
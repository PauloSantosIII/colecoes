import styled from 'styled-components'

export const StyledHeader = styled.nav`
    background: #282c34;
    width: 100%;
    position: fixed;
    top: 0;
    padding: 5px;
    z-index: 10;
    border-bottom: 4px double #ff3d00;
    height: 5vh;
    font-size: calc(20px + 2vmin);

    svg {
        margin-left: 20px;
        margin-right: 20px;
    }
`
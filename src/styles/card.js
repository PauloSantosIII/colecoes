import styled from 'styled-components'
import {Card} from 'antd'

export const StyledCard = styled(Card)`
margin: 10px;
width: 240px;
height: 320px;
border: 2px solid #fff;
font-size: 20px;

img{
    width: 240px;
}
`
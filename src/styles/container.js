import styled from 'styled-components'
import {motion} from 'framer-motion'

export const Container = styled(motion.body)`
    text-align: center;
    background: #282c34;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: calc(10px + 2vmin);
    color: white;
`
import {Container} from './container'
import {StyledHeader} from './header'
import {StyledMain} from './main'
import {StyledFooter} from './footer'
import {StyledCard} from './card'

export {Container, StyledHeader, StyledMain, StyledFooter, StyledCard}
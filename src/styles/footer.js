import styled from 'styled-components'

export const StyledFooter = styled.footer`
    background: #282c34;
    width: 100%;
    position: fixed;
    bottom: 0;
    padding: 5px;
    z-index: 10;
    height: 5vh;
    border-top: 4px double #ff3d00;
    color: #fff;
    font-size: calc(10px + 1vmin);
    text-align: center;

    a {
        color: #fff;
        margin: 20px;
        font-size: calc(12px + 2vmin);
    }

    button {
        position: fixed;
        right: 5vw;
        background: #ff3d00;
        height: 40px;
        width: 160px;
        font-weight: bolder;
    }

    @media (max-width: 620px) {
        button {
            width: 80px;
            font-weight: normal;
        }
    }
`
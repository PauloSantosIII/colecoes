import styled from 'styled-components'
import { motion } from "framer-motion"

export const StyledMain = styled(motion.main)`
    background: #282c34;
    width: 80vw;
    top: 5vh;
    bottom: 4vh;
    z-index: 0;
    min-height: 90vh;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    margin-top: 4vh;
    margin-bottom: 4vh;

    button {
        position: fixed;
        top: 13vh;
        height: 4vh;
        width: 12vw;
        color: #ff3d00;
        background: #282c34;
        border: 1px solid #ff3d00;
        border-radius: 4px;
    }

    @media (max-width: 620px) {
        button {
            width: 30vw;
        }
    }
`
import React, {useEffect, useState} from 'react'
import {PageDefault} from '../components'
import {useParams, Link, useHistory} from 'react-router-dom'
import {StyledFooter} from '../styles'
import {CharacterList} from '../components'
import {AiFillFastBackward, AiOutlineStepBackward, AiFillStepForward, AiFillFastForward} from 'react-icons/ai'
import {Button} from 'antd'

const Characters = ({setCharacters}) => {
    const {page} = useParams()
    const [characters, setCharactersAPI] = useState([])
    const history = useHistory()

    const handleOnSelect = (newCharacter) => {
        setCharacters((prevState) => prevState.find((character) => character.name === newCharacter.name)
        ? prevState
        : [...prevState, newCharacter])
    }

    useEffect(() => {
        document.title = 'Rick and Morty List'
        if(page < 1) return history.push('/rick-and-morty/1')
        if(page > 34) return history.push('/rick-and-morty/34')

        fetch(`https://rickandmortyapi.com/api/character/?page=${page}`)
        .then((res) => res.json())
        .then(({results}) => setCharactersAPI(results || []))
        console.log(page)
    }, [page, history])

    return (
    <>
    <PageDefault>
        <CharacterList onSelect={handleOnSelect} characters={characters} />
    </PageDefault>

    <StyledFooter>
        <Link to={`/rick-and-morty/1`}><AiFillFastBackward /></Link>
        <Link to={`/rick-and-morty/${Number(page) - 1}`}><AiOutlineStepBackward /></Link>
        {page}
        <Link to={`/rick-and-morty/${Number(page) + 1}`}><AiFillStepForward /></Link>
        <Link to={`/rick-and-morty/34`}><AiFillFastForward /></Link>
        <Button onClick={() => history.push('/pokemon/1')}>Go to Pokemons</Button>
    </StyledFooter>
    </>
    )
}

export default Characters
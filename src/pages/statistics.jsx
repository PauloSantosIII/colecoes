import React from 'react'
import {PageDefault} from '../components'
import {Pie} from 'react-chartjs-2'

const Statistics = ({characters, pokemons}) => {
    document.title = 'Statistics'

    const charactersData = characters.reduce((current, {species}) => { current[species] ? (current[species] += 1) : (current[species] = 1)
    return current
    }, {})

    const pokemonsData = pokemons.reduce((current, {species}) => { current[species] ? (current[species] += 1) : (current[species] = 1)
    return current
    }, {})

    const data = {
        labels: Object.keys(charactersData),
        datasets: [
            {
                data: Object.values(charactersData),
                backgroundColor: ["#ff3d00", "#36A2EB", "#FFCE56"],
                hoverBackgroundColor: ["#ff3d00", "#36A2EB", "#FFCE56"],
            },
        ],
    }

    const dataP = {
        labels: Object.keys(pokemonsData),
        datasets: [
            {
                data: Object.values(pokemonsData),
                backgroundColor: ["#FF6384", "#36A2EB", "#FFCE56"],
                hoverBackgroundColor: ["#FF6384", "#36A2EB", "#FFCE56"],
            },
        ],
    }


    return (
        <PageDefault>
            <h3>Statistics</h3>
            <Pie data={data} />
            <Pie dataP={dataP} />
        </PageDefault>
    )
}

export default Statistics
import React, {useEffect, useState} from 'react'
import {PageDefault} from '../components'
import {useParams, Link, useHistory} from 'react-router-dom'
import {StyledFooter} from '../styles'
import {PokemonList} from '../components'
import {AiFillFastBackward, AiOutlineStepBackward, AiFillStepForward, AiFillFastForward} from 'react-icons/ai'
import {Button} from 'antd'

const Pokemons = ({setPokemons}) => {
    const {offset} = useParams()
    const [pokemons, setPokemonsAPI] = useState([])
    const history = useHistory()

    const handleOnSelect = (newPokemon) => {
        setPokemons((prevState) => prevState.find((pokemon) => pokemon.name === newPokemon.name)
        ? prevState
        : [...prevState, newPokemon])
    }

    useEffect(() => {
        document.title = 'Pokemon List'
        if(offset < 1) return history.push('/pokemon/1')
        if(offset > 8) return history.push('/pokemon/8')

        fetch(`https://pokeapi.co/api/v2/pokemon?offset=${offset}&limit=20`)
            .then((res) => res.json())
            .then((res) => {
                const response = res.results.map((pok) => {
                    const brokenUrl = pok.url.split('/')

                    return {
                        name: pok.name,
                        image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${brokenUrl[brokenUrl.length - 2]}.png`,
                        species: 'pokemon',
                    }
                })
                setPokemonsAPI(response || [])
            })
    }, [offset, history])

    return (
    <>
    <PageDefault>
        <PokemonList onSelect={handleOnSelect} pokemons={pokemons} />
    </PageDefault>

    <StyledFooter>
        <Link to={`/pokemon/1`}><AiFillFastBackward /></Link>
        <Link to={`/pokemon/${Number(offset) - 1}`}><AiOutlineStepBackward /></Link>
        {offset}
        <Link to={`/pokemon/${Number(offset) + 1}`}><AiFillStepForward /></Link>
        <Link to={`/pokemon/10`}><AiFillFastForward /></Link>
        <Button onClick={() => history.push('/rick-and-morty/1')}>Go to Rick and Morty</Button>
    </StyledFooter>
    </>
    )
}

export default Pokemons
import Home from './home'
import Characters from './characters'
import Pokemons from './pokemon'
import Statistics from './statistics'

export {Home, Characters, Pokemons, Statistics}
import React, {useState} from 'react'
import {PageDefault, CharacterList, PokemonList} from '../components'
import {Button} from 'antd'

const Home = ({characters, setCharacters, pokemons, setPokemons}) => {
    const [show, setShow] = useState(true)
    const [messageButton, setMessageButton] = useState(true)

    const handleOnClick = () => {
        setShow(!show)
        setMessageButton(!messageButton)
    }
 
    const handleOnSelect = ({name}) => {
        setCharacters(characters.filter((character) => character.name !== name))
        setPokemons(pokemons.filter((pokemon) => pokemon.name !== name))
    }

    document.title = 'Collections'

    return (
        <PageDefault>
            <h3>Sua coleção</h3>
            <Button onClick={handleOnClick}>{messageButton ? 'Rick and Morty' : 'Pokemon'}</Button>
            {show 
            ? <CharacterList onSelect={handleOnSelect} characters={characters} />
            : <PokemonList onSelect={handleOnSelect} pokemons={pokemons} />}
        </PageDefault>
    )
}

export default Home